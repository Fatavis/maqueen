# The MIT License (MIT)
# Copyright (c) 2016 British Broadcasting Corporation.
# This software is provided by Lancaster University by arrangement with the BBC.
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
# Olivier Lecluse
# Avril 2019

#Modifié par Victor Lohézic Février 2021

import microbit
import time
from math import *

class Maqueen():
    def __init__(self):
        """Initiaisation robot
        addr : adresse i2c. 0x10 par defaut"""
        self.addr = 0x10
        self._speed = 100  # vitesse entre 0 et 100

    def get_speed(self):
        return self._speed

    def set_speed(self, s):
        self._speed = s

    def motorR(self, s=None):
        if s is None:
            s = self._speed
        direction = 0 if s >= 0 else 1  # sens moteur
        spd = abs(s)*255//100   # vitesse moteur 0..255
        microbit.i2c.write(self.addr, bytearray([2, direction, spd]))

    def motorL(self, s=None):
        if s is None:
            s = self._speed
        direction = 0 if s >= 0 else 1  # sens moteur
        spd = abs(floor(4*s/5)+1)*255//100   # vitesse moteur 0..255
        microbit.i2c.write(self.addr, bytearray([0, direction, spd]))

    def move_forward(self, s=None):
        if s is not None:
            self._speed = s
        self.motorR()
        self.motorL()

    def move_back(self):
        self.motorR(-self._speed)
        self.motorL(-self._speed)

    def stop(self):
        microbit.i2c.write(self.addr, bytearray([0, 0, 0]))
        microbit.sleep(1)
        microbit.i2c.write(self.addr, bytearray([2, 0, 0]))

maqueen = Maqueen()
while True:
    i = 0
    while i < 4:
        maqueen.move_forward(10)
        time.sleep_ms(1000)
        i += 1
    i = 0
    while i < 4:
        maqueen.move_back()
        time.sleep_ms(1000)
        i += 1
